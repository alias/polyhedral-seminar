# Polyhedral Compilation at a Glance

_academic year 2022-23_

[Christophe Alias](http://perso.ens-lyon.fr/christophe.alias/index_en.html)

# Goals

The polyhedral model is a powerful framework to develop
state-of-the-art code optimizations and was applied successfully to
automatic parallelization of loop kernels for high-performance and
embedded computing. This seminar will outline the general principles
of the polyhedral model on a dynamic programming kernel and discuss the
research perpectives.

* [slides](slides.pdf)
* [handout](handout.pdf)


# Resources

* [iscc](resources/iscc.tgz) [[online version]](https://compsys-tools.ens-lyon.fr/iscc/index.php), [iscc documentation](resources/iscc-manual.pdf) (see pages 6-21), [iscc tutorial](resources/iscc-tutorial.pdf).
* [demo files](resources/demo.tgz)
* A more complete [PhD course](https://gitlab.inria.fr/alias/course-poly), for the curious.

# Internships

Here is a non-limitative list of internships, all of them belonging to a
long term PhD project. Feel free to contact me if your dream topic is
not listed.
* [Compile-time Data Placement for HBM](stages/hbm-l3.pdf), in collaboration with Tohoku University, Japan.
* [Compiler-Guided Runtime Scheduling](stages/runtime-l3.pdf)
* [Dynamic Analysis of Sparse Code](stages/sparse-l3.pdf)